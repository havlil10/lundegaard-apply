package com.lunegaardApply.service;

import com.lunegaardApply.dao.RequestDao;
import com.lunegaardApply.model.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class RequestService {

    private final RequestDao requestDao;

    private final Logger logger;

    @Autowired
    public RequestService(RequestDao requestDao) {
        this.requestDao = requestDao;
        this.logger = LoggerFactory.getLogger("lundegaardApply.service.RequestService");
    }

    private boolean isWholeRequestFIlled(Request request){
        return request.getPolicyNumber() != null
                && request.getName() != null
                && request.getSurname() != null
                && request.getRequestMessage() != null
                && !request.getPolicyNumber().isEmpty()
                && !request.getName().isEmpty()
                && !request.getSurname().isEmpty()
                && !request.getRequestMessage().isEmpty();
    }

    private boolean isStringAlphanumeric(String input) {
        return input.matches("^[a-zA-Z0-9]*$");
    }

    private boolean hasStringOnlyLetters(String input) {
        char[] chars = input.toCharArray();

        for (char c : chars) {
            if(!Character.isLetter(c)) {
                return false;
            }
        }

        return true;
    }

    private boolean hasStringLessLetters(String input, int limit) {
        return input.length() < limit;
    }

    private boolean isRequestValid(Request request) {
        return isWholeRequestFIlled(request)
                && isStringAlphanumeric(request.getPolicyNumber())
                && hasStringOnlyLetters(request.getName())
                && hasStringOnlyLetters(request.getSurname())
                && hasStringLessLetters(request.getRequestMessage(), 1000);
    }

    @Transactional
    public void handleRequest(Request request) {
        if(isRequestValid(request)) {
            requestDao.save(request);
        } else {
            logger.warn("Refused to save invalid customer request");
        }
    }
}

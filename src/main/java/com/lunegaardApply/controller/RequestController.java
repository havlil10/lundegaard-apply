package com.lunegaardApply.controller;

import com.lunegaardApply.model.Request;
import com.lunegaardApply.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/rest")
public class RequestController {

    private final RequestService requestService;

    @Autowired
    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }


    @PostMapping(value = "/send")
    public void sendRequest(@RequestBody Request request) {
        requestService.handleRequest(request);
    }



}

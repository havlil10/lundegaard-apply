package com.lunegaardApply.model;

public enum RequestType {
    CONTRACT_ADJUSTMENT, DAMAGE_CASE, COMPLAINT
}

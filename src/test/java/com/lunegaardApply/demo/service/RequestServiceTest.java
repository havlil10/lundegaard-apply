package com.lunegaardApply.demo.service;

import com.lunegaardApply.model.Request;
import com.lunegaardApply.model.RequestType;
import com.lunegaardApply.service.RequestService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
public class RequestServiceTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private RequestService requestService;


    @Test
    public void createValidRequest_isPersisted() {

        Request request = new Request();
        request.setPolicyNumber("1234p");
        request.setName("Josef");
        request.setSurname("Marek");
        request.setRequestMessage("Hello 123");


        requestService.handleRequest(request);
        Request resultRequest = em.find(Request.class, request.getId());


        assertNotNull(resultRequest);
        assertEquals(request.getId(), resultRequest.getId());
    }


    @Test
    public void newRequest_withInvalidInputs_isNotSaved() {
        Request invalidRequest1 = new Request(1L, RequestType.COMPLAINT, "asd1!!!", "Josef", "Marek", "Hello 123");
        Request invalidRequest2 = new Request(2L, RequestType.COMPLAINT, "asd1", "Josef2", "Marek", "Hello 123");
        Request invalidRequest3 = new Request(3L, RequestType.COMPLAINT, "asd1!!!", "Josef", "Marek35", "Hello 123");

        String veryLongString = "a".repeat(1500);
        Request invalidRequest4 = new Request(4L, RequestType.COMPLAINT, "asd1!!!", "Josef", "Marek", veryLongString);

        requestService.handleRequest(invalidRequest1);
        requestService.handleRequest(invalidRequest2);
        requestService.handleRequest(invalidRequest3);
        requestService.handleRequest(invalidRequest4);

        assertNull(em.find(Request.class, invalidRequest1.getId()));
        assertNull(em.find(Request.class, invalidRequest2.getId()));
        assertNull(em.find(Request.class, invalidRequest3.getId()));
        assertNull(em.find(Request.class, invalidRequest4.getId()));

    }


}

import React, { Component } from 'react';
import axios from 'axios';

export class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            name: '',
            surname: '',
            requestType: 'CONTRACT_ADJUSTMENT',
            policyNumber: '',
            requestMessage: ''
        }
    }

    handleNameChange = event => {
        this.setState({
            name: event.target.value
        })
    }

    handleSurnameChange = event => {
        this.setState({
            surname: event.target.value
        })
    }

    handleTopicChange = event => {
        this.setState({
            requestType: event.target.value
        })
    }



    handlePolicyNumber = event => {
        this.setState({
            policyNumber: event.target.value
        })
    }

    handleTextareaChange = event => {
        this.setState({
            requestMessage: event.target.value
        })
    }

    handleSubmit = event => {

        axios.post('http://localhost:8080/rest/send', this.state)
            .then(response => {
                console.log(response)
            })
            .catch(error => {
                console.log(error)
            })
            
        alert("Tried to send request (no validation on frontend)")
        
    }

    render() {
        return (
            <div>
            
            <h1>Contact us</h1>

            <form onSubmit={this.handleSubmit}>
                <div className="formWrapper">
                <div>
                    <label>Kind of request</label>
                    <select value={this.state.requestType} onChange={this.handleTopicChange}>
                        <option value="CONTRACT_ADJUSTMENT">Contract adjustment</option>
                        <option value="DAMAGE_CASE">Damage case</option>
                        <option value="COMPLAINT">Complaint</option>
                    </select>
                </div>
                
                <div>    
                    <label>Policy number</label>
                    <input value={this.state.policyNumber} onChange={this.handlePolicyNumber}/>
                </div>
                <div>
                    <label>Name</label>
                    <input type='text' value={this.state.name}
                    onChange={this.handleNameChange}/>
                </div>
                <div>    
                    <label>Surname</label>
                    <input value={this.state.surname} onChange={this.handleSurnameChange}/>
                </div>
                <div>    
                    <label>Your request</label>
                    <textarea value={this.state.requestMessage} onChange={this.handleTextareaChange}
                    rows="6"></textarea>
                </div>
                
                
                <button type="submit" className="submitButton">SEND REQUEST</button>
                </div>
            </form>
            </div>
        )
    }
}

export default Form
# Vypracování zadání - formulář



## Technologie

- Backend: java, spring, jpa, REST, Postgresql
- Frontend: React(učil jsem se od základů)

## Poznámky k vypracování

- Počet člověkohodin: 13h (učil jsem se React)
- Vše funguje (obsah formuláře se uloží do postgresql databáze, pokud projde validace na backendu)
- Aplikace se může zdát rozsáhlejší - je to, protože je připravená pro další vývoj
- Ve wireframe je input field bez názvu - doptal bych se jestli tam má být
- Chybí počítadlo znaků v "Your request"
- Chybí validace na frontendu

## Poznámky ke spuštění

- projekt se musí zkompilovat a pak spustit Mavenem - aplikace běží na localhost:8080
- aby fungovalo ukládání do databáze, musí na počítači být nainstalován postgresql
- přihlašovací údaje uživatele do databáze jde změnit v src/main/resources/application.properties

